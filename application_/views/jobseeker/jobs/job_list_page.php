<style>
#pagination{
	margin: 40 40 0;
}
ul.tsc_pagination li a {
	border:solid 1px;
	border-radius:3px;
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	padding:6px 9px 6px 9px;
}
ul.tsc_pagination li {
	padding-bottom:1px;
}
ul.tsc_pagination li a:hover,
ul.tsc_pagination li a.current {
	color:#FFFFFF;
	box-shadow:0px 1px #EDEDED;
	-moz-box-shadow:0px 1px #EDEDED;
	-webkit-box-shadow:0px 1px #EDEDED;
}
ul.tsc_pagination {
	margin:4px 0;
	padding:0px;
	height:100%;
	overflow:hidden;
	font:12px 'Tahoma';
	list-style-type:none;
}
ul.tsc_pagination li {
	float:left;
	margin:0px;
	padding:0px;
	margin-left:5px;
}
ul.tsc_pagination li a {
	color:black;
	display:block;
	text-decoration:none;
	padding:7px 10px 7px 10px;
}
ul.tsc_pagination li a img {
	border:none;
}
ul.tsc_pagination li a {
	color:#0A7EC5;
	border-color:#8DC5E6;
	background:#F8FCFF;
}
ul.tsc_pagination li a:hover,
ul.tsc_pagination li a.current {
text-shadow:0px 1px #388DBE;
border-color:#3390CA;
background:#58B0E7;
background:-moz-linear-gradient(top, #B4F6FF 1px, #63D0FE 1px, #58B0E7);
background:-webkit-gradient(linear, 0 0, 0 100%, color-stop(0.02, #B4F6FF), color-stop(0.02, #63D0FE), color-stop(1, #58B0E7));
}
</style>
<?php $assets_url = site_url('assets/frontend/');?>
<!--start form -->
<?php $attributes = array('id' => 'search_job', 'method' => 'post', 'class'=> 'search-bar inner-search-bar');
     echo form_open('jobs/search',$attributes);?>
 
	<div class="container">
    	<ul class="search-form-area">
        	<li><i class="fa fa-search"></i></li>
            <li><input name="job_title" value="<?php if(isset($search_value['title'])) echo str_replace('-', ' ', $search_value['title']); ?>" placeholder="<?=trans('what_looking')?>" class="text-field-box"></li>
            <li>
            	<select class="text-field-box">
                	<option>All Category</option>
                    <option>Category one</option>
                    <option>Category two</option>	
                </select>
            </li>
            <li>
            	<select class="text-field-box">
                	<option>All Locations</option>
					<?php foreach($countries as $country):?>
					  <?php if($search_value['country'] == $country['id']): ?>
					    <option value="<?= $country['id']; ?>" selected> <?= $country['name']; ?> </option>
					  <?php else: ?>
					    <option value="<?= $country['id']; ?>"> <?= $country['name']; ?> </option>
					<?php endif; endforeach; ?>
                </select>
            </li>
            <li><input type="submit" class="button-medium" name="search" class="btn btn-info" value="<?=trans('btn_search_text')?>">
             </li>	
        </ul>
    </div>
<?php echo form_close(); ?>
<div style="clear:both;"></div>
<!-- start blog section -->

<section class="jobs-sec job-list-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h5 class="main-title">Browse Jobs</h5>
			 
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-4 order-2 sidebar search">
				<?php $attributes = array('id' => 'post_job', 'method' => 'post');
        			echo form_open('jobs/search',$attributes);?>

        			<div class="accordion job-list-accordian" id="accordionExample">
					  <div class="single-slidebar">
					    <div class="card-header" id="headingOne">
					      <h2 class="mb-0">
					        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					          <?=trans('category')?>
					        </button>
					      </h2>
					    </div>
					    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
					      <div class="card-body">
					        <ul class="cat-list">
								<?php $category_id = (isset($search_value['category']))? $search_value['category']: '';  ?>
								<?php foreach($categories as $category): ?>
									<?php if($category_id == $category['id']): ?>
									<li>
										<p><input type="checkbox" name="category" value="<?= $category['id']?>" checked="" > <?= $category['name']?></p>
									</li>
									<?php else: ?>
			                        <li>
										<p><input type="checkbox" name="category" value="<?= $category['id']?>" > <?= $category['name']?></p>
									</li>
			                    <?php endif; endforeach; ?>
							</ul>
					      </div>
					    </div>
					  </div>

					  <div class="single-slidebar">
					    <div class="card-header" id="headingTwo">
					      <h2 class="mb-0">
					        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
					          <?=trans('experience')?>
					        </button>
					      </h2>
					    </div>
					    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
					      <div class="card-body">
					        <ul class="cat-list">
								<?php $experience = (isset($search_value['experience']))? $search_value['experience']: ''; 
								?>
								<li>
									<p><input type="checkbox" name="experience" value="0-1" <?= ($experience == '0-1')? 'checked' : '' ?> > 0-1 Year </p>
								</li>
								
								<li>
									<p><input type="checkbox" name="experience" value="1-2" <?= ($experience == '1-2') ? 'checked' : '' ?>> 1-2 Years</p>
								</li>
								<li>
									<p><input type="checkbox" name="experience" value="2-5" <?= ($experience == '2-5') ? 'checked' : '' ?> > 2-5 Years</p>
								</li>
								<li>
									<p><input type="checkbox" name="experience" value="5-10" <?= ($experience == '5-10') ? 'checked' : '' ?> > 5-10 Years</p>
								</li>
								<li>
									<p><input type="checkbox" name="experience" value="10-15" <?= ($experience == '10-15') ? 'checked' : '' ?> > 10-15 Years</p>
								</li>
								<li>
									<p><input type="checkbox" name="experience" value="15+" <?= ($experience == '15+') ? 'checked' : '' ?> > 15+ Years</p>
								</li>
							</ul>
					      </div>
					    </div>
					  </div>

					  <div class="single-slidebar">
					    <div class="card-header" id="headingThree">
					      <h2 class="mb-0">
					        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
					          <?=trans('job_type')?>
					        </button>
					      </h2>
					    </div>
					    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
					      <div class="card-body">
					        <?php 
								$job_type = (isset($search_value['job_type']))? $search_value['job_type']: '';
								$types = get_job_type_list();  
							?>
							<ul class="cat-list">
								<?php foreach ($types as $type): ?>
								<li><p><input type="checkbox" name="job_type" value="<?= $type['id'] ?>" <?= ($job_type == $type['id']) ? 'checked' : '' ?> > <?= $type['type'] ?></p></li>
								<?php endforeach; ?>
							</ul>
					      </div>
					    </div>
					  </div>

					  <div class="single-slidebar">
					    <div class="card-header" id="headingFour">
					      <h2 class="mb-0">
					        <button class="btn btn-link btn-block text-left collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
					          <?=trans('emp_type')?>
					        </button>
					      </h2>
					    </div>
					    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
					      <div class="card-body">
					        <?php 
								$employment_type = (isset($search_value['employment_type']))? $search_value['employment_type']: '';  
								$emp_type = get_employment_type_list();
							?>
							<ul class="cat-list">
								<?php foreach ($emp_type as $type): ?>
								<li><p><input type="checkbox" name="employment_type" value="<?= $type['id'] ?>" <?= ($employment_type == $type['id']) ? 'checked' : '' ?> > <?= $type['type'] ?></p></li>
								<?php endforeach; ?>
							</ul>
					      </div>
					    </div>
					  </div>

					</div>

				<!-- <div class="single-slidebar">
					<h4><?=trans('category')?></h4>
					<ul class="cat-list">
						<?php $category_id = (isset($search_value['category']))? $search_value['category']: '';  ?>
						<?php foreach($categories as $category): ?>
							<?php if($category_id == $category['id']): ?>
							<li>
								<p><input type="checkbox" name="category" value="<?= $category['id']?>" checked="" > <?= $category['name']?></p>
							</li>
							<?php else: ?>
	                        <li>
								<p><input type="checkbox" name="category" value="<?= $category['id']?>" > <?= $category['name']?></p>
							</li>
	                    <?php endif; endforeach; ?>
					</ul>
				</div> -->

				<!-- <div class="single-slidebar">
					<h4><?=trans('experience')?></h4>
					<ul class="cat-list">
						<?php $experience = (isset($search_value['experience']))? $search_value['experience']: ''; 
						?>
						<li>
							<p><input type="checkbox" name="experience" value="0-1" <?= ($experience == '0-1')? 'checked' : '' ?> > 0-1 Year </p>
						</li>
						
						<li>
							<p><input type="checkbox" name="experience" value="1-2" <?= ($experience == '1-2') ? 'checked' : '' ?>> 1-2 Years</p>
						</li>
						<li>
							<p><input type="checkbox" name="experience" value="2-5" <?= ($experience == '2-5') ? 'checked' : '' ?> > 2-5 Years</p>
						</li>
						<li>
							<p><input type="checkbox" name="experience" value="5-10" <?= ($experience == '5-10') ? 'checked' : '' ?> > 5-10 Years</p>
						</li>
						<li>
							<p><input type="checkbox" name="experience" value="10-15" <?= ($experience == '10-15') ? 'checked' : '' ?> > 10-15 Years</p>
						</li>
						<li>
							<p><input type="checkbox" name="experience" value="15+" <?= ($experience == '15+') ? 'checked' : '' ?> > 15+ Years</p>
						</li>
					</ul>
				</div> -->		

			<!-- 	<div class="single-slidebar">
					<h4><?=trans('job_type')?></h4>
					<?php 
						$job_type = (isset($search_value['job_type']))? $search_value['job_type']: '';
						$types = get_job_type_list();  
					?>
					<ul class="cat-list">
						<?php foreach ($types as $type): ?>
						<li><p><input type="checkbox" name="job_type" value="<?= $type['id'] ?>" <?= ($job_type == $type['id']) ? 'checked' : '' ?> > <?= $type['type'] ?></p></li>
						<?php endforeach; ?>
					</ul>
				</div>	 -->			

				<!-- <div class="single-slidebar">
					<h4><?=trans('emp_type')?></h4>
					<?php 
						$employment_type = (isset($search_value['employment_type']))? $search_value['employment_type']: '';  
						$emp_type = get_employment_type_list();
					?>
					<ul class="cat-list">
						<?php foreach ($emp_type as $type): ?>
						<li><p><input type="checkbox" name="employment_type" value="<?= $type['id'] ?>" <?= ($employment_type == $type['id']) ? 'checked' : '' ?> > <?= $type['type'] ?></p></li>
						<?php endforeach; ?>
					</ul>
				</div> -->	
					
				<div class="single-slidebar btn-search">	
					<input type="submit" name="search" class="btn btn-info btn-block" value="<?=trans('btn_search_text');?>">
				</div>				
				<?php echo form_close(); ?>
			</div> 
			<div class="col-lg-8 order-md-2  post-list">
				<div class="col-lg-12">
				<!-- job section -->
				<div class="col-lg-8">
					<?php if(empty($jobs)){ ?>
						<div class="alert alert-danger"><strong><?=trans('sorry')?>,</strong> <?=trans('sorry_text')?></div>
					<?php } ?>
				</div>
			
				<ul class="main-job-details">
					
					
					<!-- Start:Job  -->
					<?php foreach($jobs as $job){ ?>
					<li>
					    <div class="thumb-holder">
					        <img src="<?= base_url()?>assets/img/job_icon.png" alt="<?= get_company_name($job['company_id']); ?>">
					    </div>
						<div class="info-box">
							
						<div class="details">
						 	 
							
							<div class="title-holder part-time">
								<a href="<?= site_url('jobs/'.$job['id'].'/'.($job['job_slug'])); ?>"><h6><?php echo text_limit($job['title'], 35); ?> </h6></a><span class="highlight"><i class="fa fa-bookmark"></i> <?= get_job_type_name($job['job_type']); ?></span>
								
								
							</div>
							
							
							<!--<div class="text-holder">
								<p>
									<em><?= get_company_name($job['company_id']); ?></em>
									<i class="lnr lnr-apartment"></i> <?= get_industry_name($job['industry']); ?>
									<span><i class="fa fa-map-marker"></i> <?= get_city_name($job['city']); ?>, <?= get_country_name($job['country']); ?></span> 
									<span><i class="fa fa-calendar"></i> <?= time_ago($job['created_date']); ?> </span> 
									  
										 <a class="saved_job" data-job_id="<?= $job['id']; ?>" title="Save Job"><span class="lnr lnr-heart"></span></a> 
										 <a href="<?= site_url('jobs/'.$job['id'].'/'.($job['job_slug'])); ?>" title="Apply Now" class="button-large white"><span><?=trans('apply');?></span></a> 
								</p>  
							</div>
							-->
							<div class="job-listing-footer">
							 
								<i class="lnr lnr-map-marker"></i> <?= get_city_name($job['city']); ?>, <?= get_country_name($job['country']); ?> 
								<i class="lnr lnr-apartment"></i> <?= get_industry_name($job['industry']); ?>
								<i class="lnr lnr-clock"></i> <?= time_ago($job['created_date']); ?>
							 									
						    </div>
							
						</div>	
							<div class="job-listing-btns">
					
							<a class="saved_job" title="Save Job" data-job_id="<?= $job['id']; ?>"><span class="lnr lnr-heart"></span></a>
							<a href="<?= site_url('jobs/'.$job['id'].'/'.($job['job_slug'])); ?>" title="Apply Now" class="button-white apply-job-btn"><span><?=trans('apply');?></span></a> 
						
					</div>
							
						</div>
					</li>
					<?php } ?>
					<!-- End:Job -->

					
					<!-- Show pagination links -->
					
				</ul>
			</div>
		
		</div>
		
	   </div>
	   <div class="text-center d-block w-100">
			<!-- <a href="#" class="btn browse-btn">load more</a> -->
			<?php if(count($links) > 0) { ?>
					<div id="pagination">
						<ul class="tsc_pagination">
							<?php 
								foreach($links as $link) { ?>
									<li class="page-item">
										<span class="page-link"><?php echo $link;?></span>
									</li>
								<?php } ?>
						</ul>
					</div>
					<?php } ?>
		</div>
</section>
 

