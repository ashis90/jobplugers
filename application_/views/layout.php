<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title><?=isset($title)?$title. ' - '. $this->general_settings['application_name']: 'JobPlugers'; ?></title>
	<!-- Favicon-->
	<link rel="shortcut icon" href="<?= base_url($this->general_settings['favicon']); ?>">
	<meta charset="utf-8">
	<meta name="description" content="<?= isset($meta_description) ? $meta_description : ''; ?>">
	<meta name="keywords" content="<?= isset($keywords) ? $keywords : ''; ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="canonical" href="<?= current_url() ?>">
	
	<?php if (isset($show_og_tags)): ?>
	<meta property="og:title" content="<?= $og_title; ?>">
	<meta property="og:description" content="<?= $og_description; ?>">
	<meta property="og:image" content="<?= base_url($og_image); ?>">
	<meta property="og:image:secure_url" content="<?= base_url($og_image); ?>">
	<meta property="og:url" content="<?= $og_url; ?>">
	<meta name="twitter:card" content="summary_large_image">
	<?php endif; ?>

   <!-- Frnotend-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.css"/>
	
	
	  <!--extra css-->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/linearicons.css">
	<!--extra css-->
	
	
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/frontend/css/style.css">
	
	<link href="<?= base_url(); ?>assets/frontend/css/menu.css" rel="stylesheet" type="text/css" />
 	 <!-- Frnotend End-->  
     
   
  	<script src="<?= base_url(); ?>assets/js/vendor/jquery-3.3.1.min.js"></script>
   	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-6JD9V6Q5L5"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-6JD9V6Q5L5');
</script>
</head>
 
<body class="p-0 m-0"> 

	<!-- Navbar File-->
	<?php include('navbar.php'); ?>

	<!--main content start-->
		<?php $this->load->view($layout); ?>
	<!--main content end-->

	<!-- Footer File-->
	<?php include('footer.php'); ?>


	<!-- Scripts Files -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.4.1/slick.min.js"></script>
    <script src="<?= base_url() ?>assets/frontend/js/menu.js"></script>
 
	<!-- Custom JS-->  
	<?php include('js_footer.php'); ?>
	 <script>
       $("documents").ready(function(){
          $('.slick-carousel').slick({
          	infinite: true,
			dots: true,
			arrows: false,
            slidesToShow: 4,
            speed: 1000,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 3000,
          });
        });

       $(window).scroll(function(){
	    if ($(window).scrollTop() >= 180) {
	        $('.header-area').addClass('fixed-header');
	    }
	    else {
	        $('.header-area').removeClass('fixed-header');
	    }
	});

     $('.carousel').carousel({interval: 5000 });

     jQuery(document).ready(function() {
			  // ===== Scroll to Top ==== 
		$(window).scroll(function() {
		    if ($(this).scrollTop() >= 250) {        // If page is scrolled more than 250px
		        $('#return-to-top').fadeIn(200);    // Fade in the arrow
		    } else {
		        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
		    }
		});
		$('#return-to-top').click(function() {    // When arrow is clicked
		    $('body,html').animate({
		        scrollTop : 0                       // Scroll to top of body
		    }, 500);
		});
	});
    </script>
</body>
</html>