 <!-- Start post Area -->
<!-- Start post Area -->
<section class="post-area section-gap profile_page">
  <div class="container">
    <div class="row justify-content-center d-flex">
      <div class="col-lg-4 sidebar profile-sidebar">          
        <?php $this->load->view($emp_sidebar); ?>
      </div>
      
      <div class="col-lg-8 box-shadow">
        <div class="body p-4">
          <?php if($this->session->userdata('expire')): ?>
            <p class="alert alert-info">
             <?= $this->session->userdata('expire'); ?>
            </p>
          <?php endif; ?>
          <p><?=trans('please')?> <a href="<?= base_url('employers/packages'); ?>"><?=trans('click_here')?></a> <?=trans('view_job_posting')?> <a href="<?= base_url('employers/packages'); ?>"><?=trans('packages_n_plans')?></a></p>
        </div>
        
      </div>
    </div>
  </div>
</section>