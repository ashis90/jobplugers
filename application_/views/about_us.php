<!-- start banner Area -->
        <section class="banner-area relative" id="home">  
        <div class="overlay overlay-bg"></div>
        <div class="container">
          <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
              <h1 class="text-white">
               About Us
              </h1> 
            <!--  <p class="text-white"><a href="<?= base_url(); ?>"><?=trans('label_home')?> </a>  <span class="lnr lnr-arrow-right"></span>  <a href=""> <?=trans('contact_us')?></a></p>-->
            </div>                      
          </div>
        </div>
      </section> 
      <!-- End banner Area -->  

 
      <!-- Start contact-page Area -->
   <main class="site-content">
           
            <section id="about" class="section text-center margin-bottom-50">
                <div class="container">
                    <p class="section-description">Job Pluggers was founded with a clear vision to recognize the talent and focus on acquiring the talents to the forefront, with a prime ambition and bridge the gap between the jobseekers and the job recruiters according to the requirements of the job.</p>
                    <p class="section-description">We are the fastest growing and most innovative Human Resource Consulting firm based at Kolkata, India. The company provides comprehensive services in the areas of Staff Recruiting, Temporary Staffing, Executive Staffing, Hr Outsourcing and Job Placements for leading technology companies in India.</p>
                    <p class="section-description">We also pride ourselves on providing superior talent to deliver high-quality solutions aligned with the key objectives of our client’s disciplined financial management, continuous performance improvement, and integrated technology enablement. We do so with flexibility that fit your objectives. We are excited for the future and the opportunity to work with you in these areas to exceed your expectations.</p>
                </div>                
            </section> <!-- #about -->

            <div class="clearfix"></div>


            <section id="about" class="section text-center margin-bottom-50">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-9">
                            <h2 class="section-title text-left">Our Vision</h2>

                            <p class="section-description">To offer complete corporate solutions for Human Resources of the corporate world from Manpower requirements to Human Resource Development. Achieve excellence and maintain high standards in providing total recruitment solutions to our clients & candidates with precision in process, adhering to deadlines and keeping high quality standards.</p>
                        </div>
                        <div class="col-sm-3">
                            <img src="assets/images/target(3).svg" class="img-responsive">
                        </div>
                    </div>
                </div>                
            </section> <!-- #about -->

            <div class="clearfix"></div>

            <section id="about" class="section text-center margin-bottom-50">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <img src="assets/images/mission.svg" class="img-responsive">
                        </div>
                        <div class="col-sm-9">
                            <h2 class="section-title text-left">Our Mission</h2>

                            <p class="section-description">Getting the right candidates at the right time is every HR Managers dream; we work towards achieving that dream for you. Owing to our in-depth domain knowledge and vast database, we are experts at providing the right candidate for the right job. We are engaged in offering services for Recruitment & Executive Search, Expert Training & Career Counseling, and Resume Development for Better Jobs, and more.</p>
                        </div>
                    </div>
                </div>                
            </section> <!-- #about -->
 
        </main> 
      <!-- End contact-page Area