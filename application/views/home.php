<?php $assets_url = site_url('assets/frontend/');?>
<!-- start banner_section -->
    <div class="banner_section">
      <!-- start slider -->
      <div class="slider">
        <div id="demo" class="carousel slide" data-ride="carousel">
          <!-- The slideshow -->
          <div class="carousel-inner"> 
	      <?php $attributes = array('id' => 'search_job', 'method' => 'post', 'class'=>'search-bar');
          echo form_open('jobs/search',$attributes);?>
                <div class="container">
	            	<ul class="search-form-area">
	                	<li><i class="fa fa-search"></i></li>
	                    <li><input placeholder="Key Word" class="text-field-box"></li>
	                    <li>
	                    	<select class="text-field-box">
	                        	<option>All Category</option>
	                            <option>Category one</option>
	                            <option>Category two</option>	
	                        </select>
	                    </li>
	                    <li>
	                    	<select class="text-field-box">
	                        	<option>All Locations</option>
	                            <option>Location one</option>
	                            <option>Location two</option>	
	                        </select>
	                    </li>
	                    <li><button class="button-medium">Search</button></li>	
	                </ul>
	            </div>
          <?php echo form_close(); ?> 

	        
	        
            <div class="carousel-item active">
              <div class="fill" style="background-image:url(assets/frontend/images/banner-1.jpg)"></div>
              <div class="carousel-caption">
                
              </div>
            </div>
            <div class="carousel-item">
              <div class="fill" style="background-image:url(assets/frontend/images/banner-2.jpg)"></div>
              <div class="carousel-caption">
                
              </div>
            </div>
            <div class="carousel-item">
              <div class="fill" style="background-image:url(assets/frontend/images/banner-3.jpg)"></div>
              <div class="carousel-caption">
                
              </div>
            </div>
          </div>
          <a class="carousel-control-prev" href="#demo" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#demo" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <!-- end slider -->
    </div> 
<!-- End banner_section  -->
  
  <!-- start Employers-candidate -->
	<div class="registeration-banners">
		<div class="container">
			<div class="row">
				<div class="banner-style-one col-lg-6 col-md-12 col-sm-12">
					<div class="inner-box">
                        <div class="content">
                            <h3>Employers</h3>
                            <p>Sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Labore et dolore nostrud exercitation.</p>
                            <a href="<?php echo site_url('auth/login');?>" class="theme-btn btn-style-five">Register Account</a>
                        </div>
                        <figure class="image"><img src="<?php echo $assets_url;?>images/employ.png" alt="JobPluger Register Account"></figure>
                    </div>
				</div>
				<div class="banner-style-two col-lg-6 col-md-12 col-sm-12">
					<div class="inner-box">
                        <div class="content">
                            <h3>Candidate</h3>
                            <p>Sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt. Labore et dolore nostrud exercitation.</p>
                            <a href="<?php echo site_url('auth/login');?>" class="theme-btn btn-style-five">Register Account</a>
                        </div>
                        <figure class="image"><img src="<?php echo $assets_url;?>images/candidate.png" alt="JobPluger Register Account"></figure>
                    </div>
				</div>
			</div>
		</div>
	</div>
<!-- end Employers-candidate -->
 
  <!-- Start feature-cat Area -->
  
  <section class="our-services">
	<div class="container">
		<h5 class="sub-title">FEATURED TOURS PACKAGES</h5>
		<h2 class="main-title">Browse Top Categories</h2>
		<div class="row">
			
			<?php if($categories){
				  foreach($categories as $category){
				?> 
			<div class="col-xl-2 col-lg-3 col-md-3 col-sm-4 col-6 d-flex">
                <div class="service-items">
                    <div class="services-ion">
                        <i class="<?php echo $category['icon'];?>"></i>
                    </div>
                    <div class="services-cap">
                       <h5><a href="<?= base_url('jobs/category/'.$category['slug']); ?>"><?php echo $category['name'];?></a></h5>
                       <!-- <span>(653)</span>-->
                    </div>
                </div>
            </div>
            <?php	
				}
			}
			?>
		</div>
		<div class="clearfix"></div>
        <a href="<?php echo base_url('jobs');?>" class="btn browse-btn">Browse All Sectors</a>
	</div>
</section>
  
<!-- End feature-cat Area -->

<!--start online resume -->
<section class="online-resume-section">
	<div class="container">
		<div class="row text-center justify-content-center">
			<div class="col-xl-10">
				<h5 class="sub-title">FEATURED TOURS Packages</h5>
				<h2 class="resume-title">Make a Difference with Your Online Resume!</h2>
				<a href="<?php echo base_url('auth/login');?>" class="btn resume-btn">Upload your CV</a>
			</div>
		</div>
	</div>
</section>
<!--end online resume -->
 
<!-- start feature-job -->
<section class="feature-job">
	<div class="container">
		<h5 class="sub-title">Recent Jobs</h5>
		<h2 class="main-title">Featured Jobs</h2>
		  <div class="row">
		   <?php 
		   if($jobs){
		   foreach($jobs as $job){ ?>
		 	<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 d-flex">
				<div class="single-job-item">
					<div class="job-items">
						<a href="<?php echo site_url('jobs/'.$job['id'].'/'.($job['job_slug'])); ?>" class="company-img">
							<img src="http://localhost/job_pluggers/uploads/company_logos/default.png" alt="<?= get_company_name($job['company_id']); ?>">
						</a>
					<div class="job-tittle">
						<a href="<?php echo site_url('jobs/'.$job['id'].'/'.($job['job_slug'])); ?>"><h4><?php echo text_limit($job['title'], 30); ?></h4></a>
					<h6><?= get_company_name($job['company_id']); ?></h6>   
					<ul>
                    <li><i class="lnr lnr-map-marker"></i> <?php echo get_city_name($job['city']); ?>, <?php echo get_country_name($job['country']); ?></li>
                    <li><i class="lnr lnr-apartment"></i> <?php echo get_industry_name($job['industry']); ?></li> 
                  </ul> 
						</div>
						
					</div>
					<div class="items-link f-right d-flex justify-content-between w-100 align-items-center">
                        <a href="job_details.html">Full Time</a>
                        <span><i class="lnr lnr-clock"></i> <?php echo time_ago($job['created_date']); ?></span>
                    </div>
                  
                  
                  <div class="items-link f-right d-flex justify-content-between w-100 align-items-center">
                       <a href="<?= site_url('jobs/'.$job['id'].'/'.($job['job_slug'])); ?>"><?=trans('apply');?></a>
                       <a class="saved_job" data-job_id="<?= $job['id']; ?>"><span class="lnr lnr-heart"></span></a>
                     
                  </div> 
                    	 
				</div> 
			
			</div> 
		    <?php }
		    } ?>
		    <div class="text-center d-block w-100">
				<a href="<?php echo base_url('jobs');?>" class="btn browse-btn">View All Jobs</a>
			</div>
		</div>
	</div>
</section>
<!-- end feature-job -->

<!-- start apply-process -->
<section class="apply-process text-center">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h5 class="sub-title">Here You Can See</h5>
				<h2 class="main-title">How It Works</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-6 d-flex">
				<div class="process_item">
					<i class="fas fa-search-plus"></i>
					<h6>1. Search a job</h6>
					<p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6 d-flex">
				<div class="process_item">
					<i class="fas fa-users-cog"></i>
					<h6>2. Apply for job</h6>
					<p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 col-sm-6 d-flex">
				<div class="process_item">
					<i class="fas fa-user-circle"></i>
					<h6>3. Get your job</h6>
					<p>Sorem spsum dolor sit amsectetur adipisclit, seddo eiusmod tempor incididunt ut laborea.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end apply-process -->

<!-- start about-us-->
  <section class="about-us">
    <div class="container">
      <div class="row">
      	<div class="col-md-6 align-self-center">
      		<h5 class="sub-title">WHAT WE ARE DOING</h5>
			<h2 class="main-title">24k Talented people are getting Jobs</h2>
			<h6>Mollit anim laborum duis au dolor in voluptate velit ess cillum dolore eu lore dsu quality mollit anim laborumuis au dolor in voluptate velit cillum.</h6>
			<p>Mollit anim laborum.Duis aute irufg dhjkolohr in re voluptate velit esscillumlore eu quife nrulla parihatur. Excghcepteur signjnt occa cupidatat non inulpadeserunt mollit aboru. temnthp incididbnt ut labore mollit anim laborum suis aute.</p>
			<a href="<?php echo base_url('auth/login');?>" class="btn post-btn">Post a job</a>
        </div>
        <div class="col-md-6">
          <div class="expertise-box">
            <img src="<?php echo $assets_url;?>images/support-img.jpg" alt="welcome">
            <div class="support-img-cap text-center">
	            <p>Since</p>
	            <span>2010</span>
	        </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- end about-us -->
 
  <!-- Start testimonial Area -->
<!--  <section class="testimonial-area section-full">
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="menu-content pb-60 col-lg-10">
          <div class="title text-center">
            <h1 class="mb-10"><?=trans('testimonials')?></h1>
            <p><?=trans('testimonials_subtitle')?>.</p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 shdw pt-4 pb-4">
          <div id="testimonial-slider" class="owl-carousel">
              
            <?php 
                foreach($testimonials as $row):
                    $photo = ($row['photo']) ? $row['photo'] : 'assets/img/user.png';
            ?>
              <div class="testimonial">
                <div class="pic">
                    <img src="<?= base_url($photo) ?>" alt="">
                </div>
                <h3 class="testimonial-title">
                     <?= $row['testimonial_by'] ?><small>,  <?= $row['comp_and_desig'] ?></small>
                </h3>
                <p class="description">
                    <?= $row['testimonial'] ?>
                </p>
              </div>
            <?php  endforeach; ?>
            
          </div>
        </div>
      </div>
    </div>
  </section>-->
  <!-- End testimonial Area -->
 

<!--section Bolg-->
<?php if($posts) { ?>
<section class="feature-job blog-section text-center">
	<div class="container">
		<h5 class="sub-title">Here You Can Read</h5>
		<h2 class="main-title">Our latest blog</h2>
		<div class="row">
		 <?php  
		     foreach($posts as $post){
		 	 ?>
			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 d-flex">
				<div class="single-job-item blog-item">
					<div class="job-items">
						<a href="<?= base_url().'blog/post/'.$post['slug'] ?>" class="blog-item-img" >
							<img src="<?php echo base_url($post['image_default']); ?>" alt="<?php echo $post['title']; ?>" data-featherlight="image">
						</a>
						<div class="job-tittle">
							<a href="<?= base_url().'blog/post/'.$post['slug'] ?>"><h4><?php echo $post['title']; ?></h4></a>
							<p><?php echo text_limit($post['content'], 150); ?></p>
						</div>
					</div>
					<div class="items-link text-center">
                        <a href="<?php echo base_url().'blog/post/'.$post['slug'] ?>">Read More</a>
                    </div>
				</div>
			</div>
		  <?php } ?>
			<div class="text-center d-block w-100">
				<a href="#" class="btn browse-btn">View All Blogs</a>
			</div> 
		</div> 
	</div>
</section>
 <?php } ?>
<!--Bolg-->

 
 <!-- Company section -->
 <?php if($companies){ ?>
  <section class="client-section text-center">
    <div class="container">
    	<h2 class="main-title">Top Employers</h2>
    <!-- 	<div class="slick-carousel">
             
              <?php foreach($companies as $company){ ?>
                <div class="slide_box">
                  <a href="<?php echo base_url('company/'.$company['company_slug']); ?>">
	                <div class="img_box">
	                  <img src="<?php echo base_url().$company['company_logo']; ?>" alt="<?php echo $company['company_name'];?>">
	                </div>
	                <div class="slide-content">
	                    <h6><?php echo $company['company_name'];?></h6>
	                    <a href="<?php echo base_url().$company['company_logo']; ?>" class="btn-link more-btn">Click for more details</a>
	                </div>
                  </a>
                </div>
                 <?php }?>
              </div> -->
              <div class="slick-carousel">
                <div class="slide_box">
                  <a href="javascript:void:0">
	                <div class="img_box">
	                  	<img src="images/job-logo1.png" alt="search-img">
	                </div>
	                <div class="slide-content">
	                    <h6>Model SC-2004-OC</h6>
	                    <button class="btn-link more-btn">Click for more details</button>
	                </div>
                  </a>
                </div>
                <div class="slide_box">
                  <a href="javascript:void:0">
	                <div class="img_box">
	                  	<img src="images/job-logo1.png" alt="search-img">
	                </div>
	                <div class="slide-content">
	                    <h6>Arm Garment with Shoulder</h6>
	                    <button class="btn-link more-btn">Click for more details</button>
	                </div>
                  </a>
                </div>
                <div class="slide_box">
                  <a href="javascript:void:0">
	                <div class="img_box">
	                  	<img src="images/job-logo3.png" alt="search-img">
	                </div>
	                <div class="slide-content">
	                    <h6>BIO Vest</h6>
	                    <button class="btn-link more-btn">Click for more details</button>
	                </div>
                  </a>
                </div>
                <div class="slide_box">
                  <a href="javascript:void:0">
	                <div class="img_box">
	                  	<img src="images/job-logo4.png" alt="search-img">
	                </div>
	                <div class="slide-content">
	                    <h6>Arm Garment with Shoulder</h6>
	                    <button class="btn-link more-btn">Click for more details</button>
	                </div>
                  </a>
                </div>
              	<div class="slide_box">
                  <a href="javascript:void:0">
	                <div class="img_box">
	                  	<img src="images/job-logo1.png" alt="search-img">
	                </div>
	                <div class="slide-content">
	                    <h6>Model SC-2004-OC</h6>
	                    <button class="btn-link more-btn">Click for more details</button>
	                </div>
                  </a>
                </div>
            </div>
    </div>
  </section>
  <?php }?>
<!-- end product-section -->

<!-- start cities job -->
	<div class="browse-jobs">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h5 class="sub-title">Lorem ipsum dolor sit amet elit, sed do eiusmod tempor</h5>
					<h2 class="main-title">Browse Cities Jobs</h2>
				</div>
			</div>
			<div class="row">
				<?php if($cities_job){ ?>
				<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
	    			<div class="footer-item single-slidebar">
	    				<h4>looking for Jobs</h4> 
	    				<ul class="cat-list">
	    				  <?php foreach($cities_job as $city){ ?>
	    					<li><a href="<?= base_url('jobs/location/'.get_city_slug($city['city_id'])); ?>">Jobs in <?php echo get_city_name($city['city_id']); ?><!--<span><?= $city['total_jobs']; ?></span>--></a></li> 
	    					<?php }?>
	    				</ul>
	    			</div>
	    		</div>
	    		<?php }?>
	    		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
	    			<div class="footer-item">
	    				<h4>start hiring</h4>
	    				<ul>
	    					<li><a href="javascript:void(0)">hire in Ahnedabad</a></li>
	    					<li><a href="javascript:void(0)">hire in Bengalure</a></li>
	    					<li><a href="javascript:void(0)">hire in Chandighrh</a></li>
	    					<li><a href="javascript:void(0)">hire in Delhi-NCR</a></li>
	    					<li><a href="javascript:void(0)">hire in hyderabad</a></li>
	    					<li><a href="javascript:void(0)">hire in jaipur</a></li>
	    					<li><a href="javascript:void(0)">hire in kanpur</a></li>
	    					<li><a href="javascript:void(0)">hire in kolkata</a></li>
	    					<li><a href="javascript:void(0)">hire in Lucknow</a></li>
	    					<li><a href="javascript:void(0)">hire in ladhiana</a></li>
	    					<li><a href="javascript:void(0)">hire in Mumbai</a></li>
	    					<li><a href="javascript:void(0)">hire in Pune </a></li>
	    					<li><a href="javascript:void(0)">hire in Ranchi</a></li>
	    					<li><a href="javascript:void(0)">hire in Surat</a></li>
	    				</ul>
	    			</div>
	    		</div>
	    		<div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
	    			<div class="footer-item">
	    				<h4>top jobs</h4>
	    				<ul>
	    					<li><a href="javascript:void(0)">Delivery person jobs</a></li>
	    					<li><a href="javascript:void(0)">accounts / finance jobs</a></li>
	    					<li><a href="javascript:void(0)">Sales (field work)</a></li>
	    					<li><a href="javascript:void(0)">Human Resource</a></li>
	    					<li><a href="javascript:void(0)">Backoffice jobs</a></li>
	    					<li><a href="javascript:void(0)">Business development</a></li>
	    					<li><a href="javascript:void(0)">telecesser / BPO</a></li>
	    				</ul>
	    			</div>
	    		</div>
			</div>
		</div>
	</div>
<!-- end cities job -->


