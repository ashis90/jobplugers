<!-- Start blog-posts Area -->
<section class="blog-posts-area section-gap">
	<div class="container">
		<div class="row">
			<div class="col-md-8 post-list blog-post-list">
				<h3 class="main-title">Our latest blog</h3>
				<?php 
					foreach($posts as $post): 
					$tags = get_post_tags_by_id($post['id']);
				?>

				<div class="single-post">
					<div class="img-box">
						<img class="img-fluid" src="<?= base_url($post['image_default']) ?>" alt="<?= $post['title'] ?>">
					</div>
					<ul class="tags">
						<?php foreach($tags as $tag): ?>
							<li><a href="<?= base_url('blog/tag/').$tag['tag_slug'] ?>"><?= $tag['tag'] ?></a></li>
						<?php endforeach; ?>
					</ul>

					<a href="<?= base_url('blog/post/'.$post['slug']) ?>">
						<h1><?= $post['title'] ?></h1>
					</a>
						<p>
							<?= $post['content'] ?>
						</p>
				</div>

				<?php endforeach; ?>
			</div>

			<?php $this->load->view('blog/right_nav'); ?>

		</div>
	</div>	
</section>
<!-- End blog-posts Area -->