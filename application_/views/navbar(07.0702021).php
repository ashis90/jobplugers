 <!-- header start -->
  <!-- <a href="<?= ($this->session->userdata('is_employer_login'))? base_url('employers') : base_url(); ?>"><img src="<?= base_url($this->general_settings['logo']); ?>" alt="" title="" /></a>-->
<header class="header-area">
     <nav>
			<div class="container text-center">
				<div class="row align-items-center">
					<div class="col-md-2">
						<a href="<?= base_url(); ?>" class="brand-logo" title="JobPluger">
							<span>job</span>plugers
						 </a>
					</div>
					<div class="col-md-10 d-flex text-center justify-content-end">
						<div id="cssmenu" class="menusec d-flex align-self-center text-center">
      
           <ul>
            <?php if ($this->session->userdata('is_user_login')): ?>
              <li class="menu-has-children"><a href="<?= base_url('jobs'); ?>"><?=trans('label_jobs')?></a></li>
              <li class=""><a href="<?= base_url('company'); ?>"><?=trans('label_companies')?></a></li>
              <li class=""><a href="<?= base_url('blog'); ?>"><?=trans('label_blog')?></a></li>
              <li><a href='javascript:void(0)'>About</a></li>
			  <li><a href='javascript:void(0)'>Contact</a></li>
         
                <?php 
                  $profile_picture = get_user_profile($this->session->userdata('user_id'));
                  $profile_picture = ($profile_picture) ? $profile_picture :  'assets/img/user.png';
                ?>
              <li class="menu-has-children margin-left-400"><img src="<?= base_url($profile_picture)?>" alt="user_img" height=35 /><a href="#"> <?= $this->session->userdata('username'); ?> </a>
                <ul>
                  <li><a href="<?= base_url('profile'); ?>"><?=trans('label_my_profile')?></a></li>
                  <li><a href="<?= base_url('myjobs'); ?>"><?=trans('label_my_apps')?></a></li>
                  <!--<li><a href="<?= base_url('myjobs/matching'); ?>"><?=trans('label_matching_jobs')?></a></li>
                  <li><a href="<?= base_url('myjobs/saved'); ?>"><?=trans('label_saved_jobs')?></a></li>
                  <li><a href="<?= base_url('account/change_password'); ?>"><?=trans('label_change_pass')?></a></li>-->
                  <li><a href="<?= base_url('auth/logout')?>"><?=trans('label_logout')?></a></li>
                </ul>
              </li>
            
            <?php elseif ($this->session->userdata('is_employer_login')): ?> 
            <li><a href="<?= base_url('employers/dashboard') ?>"><?=trans('label_dashboard')?></a></li>
            <li><a href="<?= base_url('employers/job/listing') ?>"><?=trans('label_manage_jobs')?></a></li>
            <li><a href="<?= base_url('employers/cv/search') ?>"><?=trans('label_find_cand')?></a></li>
            <?php 
              $profile_picture = get_employer_profile($this->session->userdata('employer_id'));
              $profile_picture = ($profile_picture) ? $profile_picture :  'assets/img/user.png';
            ?>
            <li class="menu-has-children margin-left-400"><img src="<?= base_url($profile_picture)?>" alt="user_img"  height=35 /><a href="#"> <?= $this->session->userdata('username'); ?> </a>
                <ul>
                  <!--<li><a href="<?= base_url('employers/profile') ?>"><?=trans('label_dashboard')?></a></li>
                  <li><a href="<?= base_url('employers/job/listing') ?>"><?=trans('label_manage_jobs')?></a></li>
                  <li><a href="<?= base_url('employers/account/change_password'); ?>"><?=trans('label_change_pass')?></a></li>-->
                <li><a href="<?= base_url('employers/auth/logout')?>"><?=trans('label_logout')?></a></li>
                </ul> 
            </li>
           
            <?php elseif ($this->uri->segment(1) == 'employers'): ?>   
              <li class=""><a href="<?= base_url('employers'); ?>"><?=trans('label_home')?></a></li>
              <li class=""><a href="<?= base_url('blog'); ?>"><?=trans('label_blog')?></a></li>
              <li class=""><a href="<?= base_url('employers/job/post'); ?>"><?=trans('label_post_job')?></a></li>
              <!-- <li><a class="ticker-btn-nav btn_login mt-1" href="<?= base_url('employers/auth/login') ?>"><i class="lnr lnr-user pr-1"></i> Login</a></li>
              <li><a class="nav_btn mt-1" href="<?= base_url() ?>"><i class="lnr lnr-briefcase pr-1"></i><?=trans('label_for_jobseeker')?></a> </li>-->
              
            <?php else: ?> 
                <li class="active"><a href="<?= base_url(); ?>"><?=trans('label_home')?></a></li>
                <li><a href='javascript:void(0)'>About</a></li>
                <li><a href="<?= base_url('jobs') ?>">Find a Job</a></li>
                <li><a href="<?= base_url('employers/job/post'); ?>">Post a Job</a></li> 
              <!--  <li><a href="<?= base_url('blog'); ?>">Blog</a></li> -->
			    <li><a href='javascript:void(0)'>Contact</a></li>
            <?php endif; ?>                                 
            </ul>
            
              <?php 
               if (!$this->session->userdata('is_user_login') && (!$this->session->userdata('is_employer_login'))){ ?>
               <div class="header-btn d-flex align-items-center">
                    <!--<a href="<?php echo site_url('auth/login');?>" class="btn register-btn">Register</a>-->
                    <a href="<?php echo site_url('auth/login');?>" class="btn login-btn"><i class="lnr lnr-user pr-1"></i> Login</a>
                </div>
                <?php }?>    
                    
                      </div>
						
					</div>
				</div>
			<div class="clearfix"></div>
			</div>
			</nav>
</header><!-- #header End