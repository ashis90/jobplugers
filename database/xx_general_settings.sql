-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 21, 2020 at 08:10 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onjob_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `xx_general_settings`
--

DROP TABLE IF EXISTS `xx_general_settings`;
CREATE TABLE IF NOT EXISTS `xx_general_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `favicon` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `application_name` varchar(255) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `copyright` tinytext,
  `admin_email` varchar(225) NOT NULL,
  `email_from` varchar(100) NOT NULL,
  `smtp_host` varchar(255) DEFAULT NULL,
  `smtp_port` int(11) DEFAULT NULL,
  `smtp_user` varchar(50) DEFAULT NULL,
  `smtp_pass` varchar(50) DEFAULT NULL,
  `facebook_link` varchar(255) DEFAULT NULL,
  `twitter_link` varchar(255) DEFAULT NULL,
  `google_link` varchar(255) DEFAULT NULL,
  `youtube_link` varchar(255) DEFAULT NULL,
  `linkedin_link` varchar(255) DEFAULT NULL,
  `instagram_link` varchar(255) DEFAULT NULL,
  `recaptcha_secret_key` varchar(255) DEFAULT NULL,
  `recaptcha_site_key` varchar(255) DEFAULT NULL,
  `recaptcha_lang` varchar(50) DEFAULT NULL,
  `paypal_sandbox` tinyint(4) DEFAULT NULL,
  `paypal_sandbox_url` varchar(255) DEFAULT NULL,
  `paypal_live_url` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `paypal_cur_code` varchar(5) DEFAULT NULL,
  `paypal_client_id` varchar(255) DEFAULT NULL,
  `paypal_status` tinyint(4) NOT NULL DEFAULT '1',
  `stripe_secret_key` varchar(255) DEFAULT NULL,
  `stripe_publish_key` varchar(255) DEFAULT NULL,
  `default_language` varchar(255) DEFAULT NULL COMMENT 'set from the Languages section in admin side',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `xx_general_settings`
--

INSERT INTO `xx_general_settings` (`id`, `favicon`, `logo`, `application_name`, `timezone`, `currency`, `copyright`, `admin_email`, `email_from`, `smtp_host`, `smtp_port`, `smtp_user`, `smtp_pass`, `facebook_link`, `twitter_link`, `google_link`, `youtube_link`, `linkedin_link`, `instagram_link`, `recaptcha_secret_key`, `recaptcha_site_key`, `recaptcha_lang`, `paypal_sandbox`, `paypal_sandbox_url`, `paypal_live_url`, `paypal_email`, `paypal_cur_code`, `paypal_client_id`, `paypal_status`, `stripe_secret_key`, `stripe_publish_key`, `default_language`, `created_date`, `updated_date`) VALUES
(1, 'assets/img/c3f8a7bbd04294d5e5867f1f27e0de11.png', 'assets/img/7d96e222e45339fac09959dc2d5ff4a2.png', 'OnJob.com', 'America/Adak', 'INR', 'Copyright © 2020 OnJob - All Rights Reserved.', 'info@domain.com', 'info@domain.com', 'ssl://smtp.gmail.com', 465, 'info@domain.com', 'info@domain.com', 'https://facebook.com', 'https://twitter.com', 'https://google.com', 'https://youtube.com', 'https://linkedin.com', 'https://instagram.com', '', '', 'en', 1, 'https://sandbox.paypal.com/cgi-bin/webscr', 'https://www.paypal.com/cgi-bin/webscr', 'sb-waxpi431295@business.example.com', 'USD', 'AW1tZTsRtL362R1TvXxV4J6KsXDDszBpxqLpty-dDNTpL4B3O5fOWsyGSGJ3yNGTCxqhCsZYWqEqAuc-', 1, 'sk_test_TMbUaFbCy6vreanBfGa64frP00mxfxxHiv', 'pk_test_Vq7vPui3v2LZPWCzT9LBeaVv00RAS7HwZ1', 'english', '2020-06-20 10:06:55', '2020-06-20 10:06:55');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
