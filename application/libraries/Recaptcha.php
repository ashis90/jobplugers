<?php (!defined('BASEPATH')) and exit('No direct script access allowed');
// if( ini_get('allow_url_fopen') ) {
//     die('allow_url_fopen is enabled. file_get_contents should work well');
// } else {
//     die('allow_url_fopen is disabled. file_get_contents would not work');
// }
/**

 * CodeIgniter Recaptcha library

 *

 * @package CodeIgniter

 * @author  Bo-Yi Wu <appleboy.tw@gmail.com>

 * @link    https://github.com/appleboy/CodeIgniter-reCAPTCHA

 */
class ReCaptchaResponse {
    public $success;
    public $errorCodes;
}

class Recaptcha

{

    /**

     * ci instance object

     *

     */

    private $_ci;



    /**

     * reCAPTCHA site up, verify and api url.

     *

     */

    const sign_up_url = 'https://www.google.com/recaptcha/admin';

    const site_verify_url = 'https://www.google.com/recaptcha/api/siteverify';

    const api_url = 'https://www.google.com/recaptcha/api.js';

    private static $_version        = "php_1.0";
    private static $_signupUrl      = "https://www.google.com/recaptcha/admin";
    private static $_siteVerifyUrl  = "https://www.google.com/recaptcha/api/siteverify?";
    private $_secret;
    private static $_apiUrl                = 'https://www.google.com/recaptcha/api.js';
    /**

     * constructor

     *

     * @param string $config

     */

    public function __construct()

    {

        $this->_ci = &get_instance();



        $settings = get_general_settings();

        $this->_siteKey = $settings['recaptcha_site_key'];

        $this->_secret = $settings['recaptcha_secret_key'];

        $this->_language = $settings['recaptcha_lang'];

    }

    private function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);
    
        if ($contents) return $contents;
        else return FALSE;
    }

    /**

     * Submits an HTTP GET to a reCAPTCHA server.

     *

     * @param array $data array of parameters to be sent.

     *

     * @return array response

     */

    private function _encodeQS($data)
    {
        $req = "";
        foreach ($data as $key => $value) {
            $req .= $key . '=' . urlencode(stripslashes($value)) . '&';
        }
        // Cut the last '&'
        $req=substr($req, 0, strlen($req)-1);
        return $req;
    }
    private function _submitHTTPGet($path, $data)
    {

        // $url = self::site_verify_url . '?' . http_build_query($data);

        // $response = file_get_contents($url);

        
        // return $response;

        $req = $this->_encodeQS($data);
        $response = $this->curl_get_file_contents($path . $req);
        return $response;

    }



    /**

     * Calls the reCAPTCHA siteverify API to verify whether the user passes

     * CAPTCHA test.

     *

     * @param string $response response string from recaptcha verification.

     * @param string $remoteIp IP address of end user.

     *

     * @return ReCaptchaResponse

     */

    public function verifyResponse($response, $remoteIp = null)

    {

        $remoteIp = (!empty($remoteIp)) ? $remoteIp : $this->_ci->input->ip_address();



        // Discard empty solution submissions

        if (empty($response)) {

            return array(

                'success' => false,

                'error-codes' => 'missing-input',

            );

        }


// Discard empty solution submissions
        if ($response == null || strlen($response) == 0) {
            $recaptchaResponse = new ReCaptchaResponse();
            $recaptchaResponse->success = false;
            $recaptchaResponse->errorCodes = 'missing-input';
            return $recaptchaResponse;
        }
        $getResponse = $this->_submitHttpGet(
            self::$_siteVerifyUrl,
            array(

                'secret' => $this->_secret,

                'remoteip' => $remoteIp,
                'v' => self::$_version,
                'response' => $response,

            )

        );



        // get reCAPTCHA server response
        
        $responses = json_decode($getResponse, true);

        if (isset($responses['success']) and $responses['success'] == true) {
            $status = true;
        } else {
            $status = false;
            $error = (isset($responses['error-codes'])) ? $responses['error-codes']
                : 'invalid-input-response';
        }
        return array(
            'success' => $status,
            'error-codes' => (isset($error)) ? $error : null,
        );
    }
    /**

     * Render Script Tag

     *

     * onload: Optional.

     * render: [explicit|onload] Optional.

     * hl: Optional.

     * see: https://developers.google.com/recaptcha/docs/display

     *

     * @param array parameters.

     *

     * @return scripts

     */

    public function getScriptTag(array $parameters = array())

    {

        $default = array(

            'render' => 'onload',

            'hl' => $this->_language,

        );



        $result = array_merge($default, $parameters);



        $scripts = sprintf('<script type="text/javascript" src="%s?%s" async defer></script>',

            self::$_apiUrl, http_build_query($result));



        return $scripts;

    }



    /**

     * render the reCAPTCHA widget

     *

     * data-theme: dark|light

     * data-type: audio|image

     *

     * @param array parameters.

     *

     * @return scripts

     */

    public function getWidget(array $parameters = array())

    {

        $default = array(

            'data-sitekey' => $this->_siteKey,

            'data-theme' => 'light',

            'data-type' => 'image',

            'data-size' => 'normal',

        );



        $result = array_merge($default, $parameters);



        $html = '';

        foreach ($result as $key => $value) {

            $html .= sprintf('%s="%s" ', $key, $value);

        }



        return '<div class="g-recaptcha" ' . $html . '></div>';

    }

}

