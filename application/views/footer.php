
<!-- start footer Area -->    

<?php $footer =  get_footer_settings(); ?>

 <footer>
  	<div class="layer-outer">
		<div class="shape1">
			<img src="assets/frontend/images/footer-shape-1.png" alt="Shape1">
		</div>
		<div class="shape2 aos-init">
			<img src="assets/frontend/images/footer-shape-2.png" alt="Shape2">
		</div>
	</div>
    <div class="container">
    	<div class="row">
    		<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    			<div class="footer-item">
    				<h4 class="footer-title">About Us</h4>
    				<p class="footer-description">JobPlugers understand your commitment towards success and offer to become your HR department and co-employer. We aim to provide innovative staffing solutions by bearing the burden and sharing the risk.</p>
    			</div>
    		</div>

    		<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    			<div class="footer-item">
    				<h4 class="footer-title">CONTACT INFO</h4>
    				<p class="footer-description">Address :Your address goes here, your demo address.</p>
    				<ul>
    					<li><a href="tel:9051835047">Phone : +91 9051835047</a></li>
    					<li><a href="mailto: info@jobpluger.com">Email : info@jobpluger.com</a></li>
    				</ul>
    			</div>
    		</div>

    		<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    			<div class="footer-item">
    				<h4 class="footer-title">IMPORTANT LINK</h4>
    				<ul>
    					<li><a href="<?= base_url(); ?>">Home</a></li>
    					<li><a href="<?= base_url('employers/job/listing') ?>">Find a job</a></li>
    					<li><a href="<?= base_url('employers/job/post'); ?>">Post a job</a></li>
    					<li><a href="<?= base_url('about-us'); ?>">About Us</a></li>
    					<li><a href="<?= base_url('contact'); ?>">Contact Us</a></li>
    					<li><a href="<?= base_url('terms-n-conditions'); ?>">Terms & Conditions</a></li>
    					<li><a href="<?= base_url('faq'); ?>">FAQ</a></li>
    				</ul>
    			</div>
    		</div>

    		<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
    			<div class="footer-item">
    				<h4 class="footer-title">NEWSLETTER</h4>
    				<p class="footer-description">Join our 10,000+ subscribers and get access to the latest templates, freebies, announcements and resources!</p>
    		 <!-- flash message-->
    		   <?php if ($this->session->flashdata('success_subscriber')): ?>
                <div class="m-b-15">
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <p>
                            <i class="icon fa fa-check"></i>
                            <?php echo $this->session->flashdata('success_subscriber'); ?>
                        </p>
                    </div>
                </div>
              <?php endif; ?>
    
              <?php if ($this->session->flashdata('error_subscriber')): ?>
                <div class="m-b-15">
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <p>
                            <i class="icon fa fa-check"></i>
                            <?php echo $this->session->flashdata('error_subscriber'); ?>
                        </p>
                    </div>
                </div>
              <?php endif; ?>
              <!-- flash message-->
					  <?php echo form_open(base_url('home/add_subscriber'), 'class="form-inline"');  ?> 
						<input type="text" name="subscriber_email" class="form-control" placeholder=" Email Address " required>
						<button type="submit" class="btn submit-btn">Submit</button>
					   <?php echo form_close();?>
    			 </div>
    		  </div>

    	   </div>
        </div>
     	<div class="up_copy_section text-center">
     		<p>Jobplugers does not charge any fees at any stage from the candidates</p>
     	</div>
     	<div class="copy_section text-center">
     		<p>© Copyright Jobplugers 2021. All Rights Reserved.</p>
     	</div>

<!-- 
     	<div class="scroll-top" id="return-to-top">
          <a href="javascript:"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        </div> -->
    
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<!-- End Footer Area -->

<!-- start Copyright Area -->
<!--<div class="copyright1">
  <div class="container">
    <div class="row"> 
      <div class="col-md-6 col-8">
        <div class="bottom_footer_info">
          <p><?= $this->general_settings['copyright']?></p>
        </div>
      </div>
      <div class="col-md-6 col-4">
        <div class="bottom_footer_logo text-right">
          <ul class="list-inline">
            <li class="list-inline-item"><a target="_blank" href="<?= $this->general_settings['facebook_link']; ?>"><i class="fa fa-facebook"></i></a></li>
            <li class="list-inline-item"><a target="_blank" href="<?= $this->general_settings['twitter_link']; ?>"><i class="fa fa-twitter"></i></a></li>
            <li class="list-inline-item"><a target="_blank" href="<?= $this->general_settings['google_link']; ?>"><i class="fa fa-google"></i></a></li>
            <li class="list-inline-item"><a target="_blank" href="<?= $this->general_settings['linkedin_link']; ?>"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>-->
<!-- End Copyright Area --> 