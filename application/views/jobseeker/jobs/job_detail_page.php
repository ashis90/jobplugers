<!-- JSsocials -->
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/jssocials/jssocials.css" />
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/plugins/jssocials/jssocials-theme-flat.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
<?php $assets_url = site_url('assets/frontend/');?>
<!-- start banner_section -->
     
<!-- start banner_section -->

<!-- Start post Area -->
<section class="jobs-details">
	<div class="container">
		<div class="row">
	   	     <div class="col-lg-8 col-12">
				<?php if($this->session->flashdata('applied_success')): ?>
		          <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
		            <?=$this->session->flashdata('applied_success')?>
		          </div>
		        <?php  endif; ?>
		        <?php if($already_applied == true && $this->session->flashdata('applied_success') == null): ?>
		          <div class="alert alert-success">
		            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
					  <?=trans('already_applied')?>
		          </div>
		        <?php  endif; ?>
		        <?php if($this->session->flashdata('validation_errors')): ?>
		         <div class="alert alert-danger">
		          <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
		          <?= $this->session->flashdata('validation_errors') ?>
		        </div>
		      <?php endif; ?>
			</div>
			
			<div class="col-md-8 left-sec">
			
            	<div class="company-detail">
                	<div class="logo">
                    	<img src="<?= base_url()?>assets/img/job_icon.png" alt="<?= get_company_name($job_detail['company_id']); ?>">
                    </div>
                    <div class="title">
                    	<h4><?= $job_detail['title']; ?></h4>
                        <p><i class="fa fa-link"></i> <?= get_company_name($job_detail['company_id']); ?></p>
                     <div class="title-holder part-time"><span class="highlight"><i class="fa fa-bookmark"></i> <?= get_job_type_name($job_detail['job_type']); ?></span></div>
                       
                    </div>
                </div>
                <div class="job-detail-content">
                <p><?= $job_detail['description']; ?></p></div>
                
                <!-- Start: Tab  Area -->
                <div class="card tab-section">
		        <div class="card-header">
		          <ul class="nav nav-tabs card-header-tabs" id="bologna-list" role="tablist">
		            <li class="nav-item">
		              <a class="nav-link active" data-toggle="tab" href="#requirments" role="tab" aria-controls="requirments" aria-selected="true">requirments</a>
		            </li>
		            <li class="nav-item">
		              <a class="nav-link" data-toggle="tab"  href="#skils" role="tab" aria-controls="skils" aria-selected="false">skils</a>
		            </li>
		            
		          </ul>
		        </div>
		       
		        <div class="card-body">
		           <div class="tab-content">
		            <div class="tab-pane active" id="requirments" role="tabpanel">
		              <ul class="list-style-icon-hand">
                        <li><strong><?=trans('education')?>:</strong>&nbsp; <?= get_education_level($job_detail['education']); ?></li>
                        <li><strong><?=trans('gender')?>:</strong>&nbsp; <?= $job_detail['gender']; ?></li>
                        <li><strong><?=trans('experience')?>:</strong>&nbsp; <?= $job_detail['experience'];?> Years</li>
                     </ul>
		            </div> 
		           
		            <div class="tab-pane" id="skils" role="tabpanel" aria-labelledby="skils">  
                       <?php $skills = explode("," , $job_detail['skills']);?>
						  <ul class="list-style-icon-hand">
							<?php foreach($skills as $skill): ?>
							<li>
								<a href="#"><?= $skill; ?></a>
							</li>
							<?php endforeach; ?>
						  </ul>
		            </div>
		             
		              </div>
		           </div> 
		         </div>
                <!-- End: Tab Area -->  
              
              <!-- Apply Job-->       
                <div id="apply-block">
					<div class="collapse" id="collapseExample">
						<div class="card card-body">
							<h4 class="card-title"><?=trans('apply_for_job')?></h4>
						    <?php $attributes = array('id' => 'job-form', 'method' => 'post');
		        			echo form_open(base_url('jobs/apply_job'),$attributes);
		        			?>	
						      	<div class="form-group">
							       <textarea name="cover_letter" class="form-control" rows="5" placeholder="<?=trans('msg_sect_employer')?>"></textarea>
							    </div> 
	                            
							    <!-- Hidden Inputs -->
							    <?php if($user_detail){ ?>
							    <input type="hidden" name="username" value="<?php echo $user_detail['firstname'];?>">
							    <input type="hidden" name="email" value="<?php echo $user_detail['email']; ?>">
							    <?php }
							    else { ?>
							    <input type="hidden" name="username" value="">
							    <input type="hidden" name="email" value="">
							    <?php 	}
							    ?>
							    <input type="hidden" name="job_id" value="<?= $job_detail['id']; ?>" >
							    <input type="hidden" name="emp_id" value="<?= $job_detail['employer_id']; ?>" >
							    <input type="hidden" name="job_title" value="<?= $job_detail['title']; ?>" >
							    <!-- current url for redirect to same job detail page  -->
							    <input type="hidden" name="job_actual_link" value="<?= $job_actual_link ?>" >
							 
								<?php
								    $last_request_page = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
								    $this->session->set_userdata('last_request_page', $last_request_page); 
								 ?>

								<?php if($this->session->userdata('is_user_login') == true): ?>
								    <button type="submit" name="submit" value="submit" class="btn btn-primary btn-block"><?=trans('send_application')?></button>

								<?php elseif($this->session->userdata('is_employer_login') == true): ?>
								    <a href="<?= base_url('auth/login'); ?>" class="btn btn-primary btn-block"><?=trans('login_jobseeker')?></a>
								<?php else: ?>
								    <a href="<?= base_url('auth/login'); ?>" class="btn btn-primary btn-block"><?=trans('login_jobseeker')?></a>
								<?php endif; ?>    

							<?php echo form_close(); ?>
						</div>
					</div>
				</div>
                
              <!--End Apply Job-->      
                
            </div>
            
         <!--   Job Overview-->
            <div class="col-md-4 right-sec">
            	<div class="job-overview-sec">
            		<div class="heading-dft"><h4>Job Overview</h4></div>
                    <div class="overview-box">
                    	<ul class="overview-items">
                        	
                            <li><i class="fas fa-calendar-week"></i>
                            	<span>Date Posted:</span>
								<p>Posted <?= date('d-m-Y', strtotime($job_detail['created_date'])); ?></p> 
                            </li>
                            <li><i class="fas fa-map-marker"></i>
                            	<span>Location:</span>
								<p><?= get_city_name($job_detail['city']); ?>, <?= get_country_name($job_detail['country']); ?></p>
                            </li>
                            <li><i class="fas fa-user"></i>
                            	<span><?=trans('total_positions')?>:</span>
								<p><?= $job_detail['total_positions']; ?></p>
                            </li>
                            <li><i class="fas fa-money-bill-alt"></i>
                            	<span>Salary:</span>
								<p><?= $job_detail['min_salary']; ?><?= $this->general_settings['currency']; ?> - <?= $job_detail['max_salary']; ?><?= $this->general_settings['currency']; ?>  (<?= $job_detail['salary_period']; ?>)</p>	
                            </li>
                            <li><i class="fas fa-industry"></i>
                            	<span><?=trans('industry')?>:</span>
								<p><?= get_industry_name($job_detail['industry']); ?></p> 
                            </li>
                        </ul>
                          <a class="btn apply-btn" data-toggle="collapse" href="#collapseExample"  href="javascript:void(0);"><?=trans('apply')?></a>
                          <div class="clearfix"></div>
                         
						<span class="inline-block" id="share"></span> 
                        
                    </div>
                </div> 
            </div> 
		 <!--   Job Overview-->
		</div> 
	</div>
</section>



<!-- End post Area -->

<script>
    $(document).ready(function (){
        $("#btn-apply").click(function (){
            $('html, body').animate({
                scrollTop: $("#apply-block").offset().top-90
            }, 1000);
        });
    });
</script>

<script src="<?= base_url() ?>assets/plugins/jssocials/jssocials.min.js"></script>
<script>
    $("#share").jsSocials({
     showLabel: false,
     showCount: false,
     shares: ["email","twitter", "facebook", "googleplus", "linkedin", "pinterest"]
 	});
</script>


