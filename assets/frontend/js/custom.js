$("documents").ready(function(){
          $('.slick-carousel').slick({
          	infinite: true,
			dots: true,
			arrows: false,
            slidesToShow: 4,
            speed: 1000,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 3000,
          });
        });

       $(window).scroll(function(){
	    if ($(window).scrollTop() >= 180) {
	        $('.header-area').addClass('fixed-header');
	    }
	    else {
	        $('.header-area').removeClass('fixed-header');
	    }
	});

     $('.carousel').carousel({interval: 5000 });

     jQuery(document).ready(function() {
			  // ===== Scroll to Top ==== 
		$(window).scroll(function() {
		    if ($(this).scrollTop() >= 250) {        // If page is scrolled more than 250px
		        $('#return-to-top').fadeIn(200);    // Fade in the arrow
		    } else {
		        $('#return-to-top').fadeOut(200);   // Else fade out the arrow
		    }
		});
		$('#return-to-top').click(function() {    // When arrow is clicked
		    $('body,html').animate({
		        scrollTop : 0                       // Scroll to top of body
		    }, 500);
		});
	});

$('#bologna-list a').on('click', function (e) {
  e.preventDefault()
  $(this).tab('show')
})     