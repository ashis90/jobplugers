-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 21, 2020 at 06:54 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `classify_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `xx_currency`
--

DROP TABLE IF EXISTS `xx_currency`;
CREATE TABLE IF NOT EXISTS `xx_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xx_currency`
--

INSERT INTO `xx_currency` (`id`, `name`, `code`, `symbol`) VALUES
(114, 'Dollars', 'USD', '$'),
(115, 'Afghanis', 'AFN', '؋'),
(116, 'Pesos', 'ARS', '$'),
(117, 'Guilders', 'AWG', 'ƒ'),
(118, 'Dollars', 'AUD', '$'),
(119, 'New Manats', 'AZN', 'ман'),
(120, 'Dollars', 'BSD', '$'),
(121, 'Dollars', 'BBD', '$'),
(122, 'Rubles', 'BYR', 'p.'),
(123, 'Euro', 'EUR', '€'),
(124, 'Dollars', 'BZD', 'BZ$'),
(125, 'Dollars', 'BMD', '$'),
(126, 'Bolivianos', 'BOB', '$b'),
(127, 'Convertible Marka', 'BAM', 'KM'),
(128, 'Pula', 'BWP', 'P'),
(129, 'Leva', 'BGN', 'лв'),
(130, 'Reais', 'BRL', 'R$'),
(131, 'Pounds', 'GBP', '£'),
(132, 'Dollars', 'BND', '$'),
(133, 'Riels', 'KHR', '៛'),
(134, 'Dollars', 'CAD', '$'),
(135, 'Dollars', 'KYD', '$'),
(136, 'Pesos', 'CLP', '$'),
(137, 'Yuan Renminbi', 'CNY', '¥'),
(138, 'Pesos', 'COP', '$'),
(139, 'Colón', 'CRC', '₡'),
(140, 'Kuna', 'HRK', 'kn'),
(141, 'Pesos', 'CUP', '₱'),
(142, 'Koruny', 'CZK', 'Kč'),
(143, 'Kroner', 'DKK', 'kr'),
(144, 'Pesos', 'DOP ', 'RD$'),
(145, 'Dollars', 'XCD', '$'),
(146, 'Pounds', 'EGP', '£'),
(147, 'Colones', 'SVC', '$'),
(148, 'Pounds', 'FKP', '£'),
(149, 'Dollars', 'FJD', '$'),
(150, 'Cedis', 'GHC', '¢'),
(151, 'Pounds', 'GIP', '£'),
(152, 'Quetzales', 'GTQ', 'Q'),
(153, 'Pounds', 'GGP', '£'),
(154, 'Dollars', 'GYD', '$'),
(155, 'Lempiras', 'HNL', 'L'),
(156, 'Dollars', 'HKD', '$'),
(157, 'Forint', 'HUF', 'Ft'),
(158, 'Kronur', 'ISK', 'kr'),
(159, 'Rupees', 'INR', 'Rp'),
(160, 'Rupiahs', 'IDR', 'Rp'),
(161, 'Rials', 'IRR', '﷼'),
(162, 'Pounds', 'IMP', '£'),
(163, 'New Shekels', 'ILS', '₪'),
(164, 'Dollars', 'JMD', 'J$'),
(165, 'Yen', 'JPY', '¥'),
(166, 'Pounds', 'JEP', '£'),
(167, 'Tenge', 'KZT', 'лв'),
(168, 'Won', 'KPW', '₩'),
(169, 'Won', 'KRW', '₩'),
(170, 'Soms', 'KGS', 'лв'),
(171, 'Kips', 'LAK', '₭'),
(172, 'Lati', 'LVL', 'Ls'),
(173, 'Pounds', 'LBP', '£'),
(174, 'Dollars', 'LRD', '$'),
(175, 'Switzerland Francs', 'CHF', 'CHF'),
(176, 'Litai', 'LTL', 'Lt'),
(177, 'Denars', 'MKD', 'ден'),
(178, 'Ringgits', 'MYR', 'RM'),
(179, 'Rupees', 'MUR', '₨'),
(180, 'Pesos', 'MXN', '$'),
(181, 'Tugriks', 'MNT', '₮'),
(182, 'Meticais', 'MZN', 'MT'),
(183, 'Dollars', 'NAD', '$'),
(184, 'Rupees', 'NPR', '₨'),
(185, 'Guilders', 'ANG', 'ƒ'),
(186, 'Dollars', 'NZD', '$'),
(187, 'Cordobas', 'NIO', 'C$'),
(188, 'Nairas', 'NGN', '₦'),
(189, 'Krone', 'NOK', 'kr'),
(190, 'Rials', 'OMR', '﷼'),
(191, 'Rupees', 'PKR', '₨'),
(192, 'Balboa', 'PAB', 'B/.'),
(193, 'Guarani', 'PYG', 'Gs'),
(194, 'Nuevos Soles', 'PEN', 'S/.'),
(195, 'Pesos', 'PHP', 'Php'),
(196, 'Zlotych', 'PLN', 'zł'),
(197, 'Rials', 'QAR', '﷼'),
(198, 'New Lei', 'RON', 'lei'),
(199, 'Rubles', 'RUB', 'руб'),
(200, 'Pounds', 'SHP', '£'),
(201, 'Riyals', 'SAR', '﷼'),
(202, 'Dinars', 'RSD', 'Дин.'),
(203, 'Rupees', 'SCR', '₨'),
(204, 'Dollars', 'SGD', '$'),
(205, 'Dollars', 'SBD', '$'),
(206, 'Shillings', 'SOS', 'S'),
(207, 'Rand', 'ZAR', 'R'),
(208, 'Rupees', 'LKR', '₨'),
(209, 'Kronor', 'SEK', 'kr'),
(210, 'Dollars', 'SRD', '$'),
(211, 'Pounds', 'SYP', '£'),
(212, 'New Dollars', 'TWD', 'NT$'),
(213, 'Baht', 'THB', '฿'),
(214, 'Dollars', 'TTD', 'TT$'),
(215, 'Lira', 'TRY', 'TL'),
(216, 'Liras', 'TRL', '£'),
(217, 'Dollars', 'TVD', '$'),
(218, 'Hryvnia', 'UAH', '₴'),
(219, 'Pesos', 'UYU', '$U'),
(220, 'Sums', 'UZS', 'лв'),
(221, 'Bolivares Fuertes', 'VEF', 'Bs'),
(222, 'Dong', 'VND', '₫'),
(223, 'Rials', 'YER', '﷼'),
(224, 'Zimbabwe Dollars', 'ZWD', 'Z$'),
(225, 'New Ciuurc', 'NCY', '@&323');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
