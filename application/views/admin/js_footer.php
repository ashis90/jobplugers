<script type="text/javascript">

var base_url = '<?php echo base_url(); ?>';
var csfr_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';
var csfr_token_value = '<?php echo $this->security->get_csrf_hash(); ?>';
var user_id = '<?php echo $this->uri->segment(4);?>';
//-------------------------------------------------------------------
// Country State & City Change
  $(document).on('change','.country',function()
  {
    var data =  {
      country : this.value,
    }
    data[csfr_token_name] = csfr_token_value;

    $.ajax({
      type: "POST",
      url: "<?= base_url('account/get_country_states') ?>",
      data: data,
      dataType: "json",
      success: function(obj) {
        $('.state').html(obj.msg);
     },

    });

  });

  $(document).on('change','.emp_state',function()
  {
    var data =  {
      state : this.value,
    }
    data[csfr_token_name] = csfr_token_value;
    $.ajax({
      type: "POST",
      url: "<?= base_url('account/get_state_cities') ?>",
      data: data,
      dataType: "json",
      success: function(obj) {
        $('.emp_city').html(obj.msg);
     },

    });

  });
  
  $(document).on('change','.state',function()
  {
    var data =  {
      state : this.value,
    }
    data[csfr_token_name] = csfr_token_value;
    $.ajax({
      type: "POST",
      url: "<?= base_url('account/get_state_cities') ?>",
      data: data,
      dataType: "json",
      success: function(obj) {
        $('.city').html(obj.msg);
     },

    });

  });

//-------------------------------------------------------------------
// Delete Confirm Dialogue box
$(document).ready(function(){
  $(".btn-delete").click(function(){
    if (!confirm("Are you sure? you want to delete")){
      return false;
    }
  });
});

// ---------------------------------------------------
// Close Education collapse
  $(document).on('click',".close_all_collapse",function(){
    $(".collapse").collapse('hide');
  });

// -------------------------------------------
// Edit user education
$(document).on('click','.edit-education',function(){
  var data = {
    edu_id : $(this).data('edu_id'),
    user_id: user_id
  }
  data[csfr_token_name] = csfr_token_value;
   $.ajax({
    type: 'POST',
    url: base_url + 'admin/users/get_education_by_id',
    data: data,
    success: function (response) {
      $('#user-education-edit').html(response);
      $('#user-education-edit').collapse('show');
    }

  });
});

// -------------------------------------------
// Edit user language
$(document).on('click','.edit-language',function(){
  var data = {
    lang_id : $(this).data('lang_id'),
    user_id: user_id
  }
  data[csfr_token_name] = csfr_token_value;
   $.ajax({
    type: 'POST',
    url: base_url + 'admin/users/get_language_by_id',
    data: data,
    success: function (response) {
      $('#user-language-edit').html(response);
      $('#user-language-edit').collapse('show');
    }

  });
});

//-------------------------------------
// 
// -------------------------------------------
// Edit user language
$(document).on('click','.edit-experience',function(){
  var data = {
    exp_id : $(this).data('exp_id'),
    user_id: user_id
  }
  data[csfr_token_name] = csfr_token_value;
   $.ajax({
    type: 'POST',
    url: base_url + 'admin/users/get_experience_by_id',
    data: data,
    success: function (response) {
      $('#user-experience-edit').html(response);
      $('#user-experience-edit').collapse('show');
    }

  });
});


</script>