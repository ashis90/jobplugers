<!-- start banner Area -->
      <!-- <section class="banner-area relative" id="home">  
        <div class="overlay overlay-bg"></div>
        <div class="container">
          <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
              <h1 class="text-white">
                <?=trans('contact_us')?>
              </h1> 
              <p class="text-white"><a href="<?= base_url(); ?>"><?=trans('label_home')?> </a>  <span class="lnr lnr-arrow-right"></span>  <a href=""> <?=trans('contact_us')?></a></p>
            </div>                      
          </div>
        </div>
      </section> -->
      <!-- End banner Area -->  


      <!-- map box -->
      <div class="map-box mt-5">
        <div class="container">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3685.7113281305697!2d88.39896045055204!3d22.51501144068354!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a02715563a4e44d%3A0xa901f5a0c6f9aa14!2sRuby!5e0!3m2!1sen!2sin!4v1627678293536!5m2!1sen!2sin" width="100%" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
      </div>

      <!-- Start contact-page Area -->
      <section class="contact-page-area section-gap mt-3 text-center">
        <div class="container">
          <h5 class="main-title">Get in Touch</h5>
          <div class="row">
            <div class="col-lg-4">
              <div class=" d-flex flex-column">
                <a class="contact-btns" href="<?= base_url('jobs'); ?>"><?=trans('label_search_job')?></a>
                <a class="contact-btns" href="<?= base_url('employers/job/post'); ?>"><?=trans('post_new_job')?></a>
                <a class="contact-btns" href="<?= base_url('auth/login'); ?>"><?=trans('create_job')?></a>
              </div>
              <div class="d-block text-left mt-3">
                <p>Address :Your address goes here, your demo address.</p>
                <ul>
                  <li><a href="tel:9051835047">Phone : +91 9051835047</a></li>
                  <li><a href="mailto: info@jobpluger.com">Email : info@jobpluger.com</a></li>
                </ul>
              </div>
            </div>
            <div class="col-lg-8">
              <?php if($this->session->flashdata('success')): ?>
                <div class="alert alert-success">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                  <?=$this->session->flashdata('success')?>
                </div>
              <?php  endif; ?>
         
              <?php $attributes = array('id' => '', 'method' => 'post' , 'class' => 'form-area contact-form text-right'); ?>
              <?php echo form_open('contact',$attributes);?>  
                <div class="row"> 
                  <div class="col-lg-12 form-group text-left">
                    <input name="username" placeholder="Enter your name" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?=trans('enter_your_name')?>'" class="common-input mb-20 form-control" required="" type="text">
                  
                    <input name="email" placeholder="Enter email address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?=trans('enter_email')?>'" class="common-input mb-20 form-control" required="" type="email">

                    <input name="subject" placeholder="Enter your subject" onfocus="this.placeholder = ''" onblur="this.placeholder = '<?=trans('subject')?>'" class="common-input mb-20 form-control" required="" type="text">

                    <textarea class="common-textarea mt-10 form-control" name="message" placeholder="<?=trans('message')?>" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Messege'" required=""></textarea>
                    <input type="submit" name="submit" value="<?=trans('send_message')?>" class="btn mt-5"/>
                  </div>
                </div>
              </form> 
            </div>
          </div>
        </div>  
      </section>
      <!-- End contact-page Area